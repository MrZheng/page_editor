$(function(){
    var currentTarget = $("#J_UICodePanel");
    var relations = {
        "J_UICodePanel": $("#J_Edit_bg").html()
    };
    var formData = {
        "J_UICodePanel": []
    };
    var editForm = $("#J_EditForm");
    var canvas = $("#J_UICodePanel");
    var resizeMain = $("#J_Tpl_resize").html();
    var isTarget = false, isTargetTime;
    var deleteBar = $("#J_DeleteBar");

    editForm.find("input[type='text']").each(function(i, o){
        formData["J_UICodePanel"][i] = $(o).val();
    });

    // 生成唯一ID
    function guid() {
        function G() {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
        }
        return (G() + G()).toUpperCase();
    }

    function hasOwnProperty(prop, obj) {
        if (obj.hasOwnProperty) {
            return obj.hasOwnProperty(prop);
        } else {
            return (prop in obj);
        }
    }

    // IE 等不支持 onchange 冒泡的浏览器，无效
    editForm.delegate("select", "change", function(e){
        $(this).trigger("focusout");
    });

    editForm.delegate("input[type='radio']", "click", function(e){
        var _this = this;
        setTimeout(function(){
            if (!$(_this).attr("data-elem")) {
                // currentTarget.css($(_this).attr("name"), $(_this).val() + "px");
                if (($(_this).attr("name") === "background" || $(_this).attr("name") === "backgroundImage") && /^http:\/\//g.test($(_this).val())) {
                    currentTarget.css($(_this).attr("name"), "url(" + $(_this).val() + ")");
                } else {
                    currentTarget.css($(_this).attr("name"), $(_this).val() + "px");
                }
            } else {
                var ele = currentTarget.find("." + $(_this).attr("data-elem"));
                if ($(_this).attr("data-param") === "target") {
                    ele.attr("target", $(_this).val());
                } else if ($(_this).attr("data-param") === "link") {
                    ele.attr("href", $(_this).val());
                } else if ($(_this).attr("data-param") === "text") {
                    ele.html($(_this).val());
                } else if ($(_this).attr("data-param") === "style"){
                    var prop = $(_this).attr("data-prop");
                    var prop_val = $(_this).val();
                    if ((prop === "background" || prop === "backgroundImage") && /^http:\/\//g.test(prop_val)) {
                        ele.css($(_this).attr("data-prop"), "url(" + $(_this).val() + ")");
                    }else if ((prop === "line-height" || prop === "font-size")) {
                        ele.css($(_this).attr("data-prop"), $(_this).val() + "px");
                    }else {
                        ele.css($(_this).attr("data-prop"), $(_this).val());
                    }
                }
            }
        }, 100);
    });

    // 代理处理blur事件，设置currentTarget的各项属性
    editForm.delegate(".J_CanBlur", "focusout", function(e){
        if (isTargetTime) {
            clearTimeout(isTargetTime);
            isTargetTime = null;
        }
        if (!isTarget) {
            var stdin = $(".J_CanBlur");
            stdin.each(function(index, obj){
                if (!$(obj).attr("data-elem")) {
                    // currentTarget.css($(obj).attr("name"), $(obj).val() + "px");
                    if (($(obj).attr("name") === "background" || $(obj).attr("name") === "backgroundImage") && /^http:\/\//g.test($(obj).val())) {
                        currentTarget.css($(obj).attr("name"), "url(" + $(obj).val() + ")");
                    } else {
                        currentTarget.css($(obj).attr("name"), $(obj).val() + "px");
                    }
                } else {
                    var ele = currentTarget.find("." + $(obj).attr("data-elem"));
                    if ($(obj).attr("data-param") === "target") {
                        ele.attr("target", $(obj).val());
                    } else if ($(obj).attr("data-param") === "link") {
                        ele.attr("href", $(obj).val());
                    } else if ($(obj).attr("data-param") === "text") {
                        ele.html($(obj).val());
                    } else if ($(obj).attr("data-param") === "style"){
                        var prop = $(obj).attr("data-prop");
                        var prop_val = $(obj).val();
                        if ((prop === "background" || prop === "backgroundImage") && /^http:\/\//g.test(prop_val)) {
                            ele.css($(obj).attr("data-prop"), "url(" + $(obj).val() + ")");
                        }else if ((prop === "line-height" || prop === "font-size")) {
                            ele.css($(obj).attr("data-prop"), $(obj).val() + "px");
                        }else {
                            ele.css($(obj).attr("data-prop"), $(obj).val());
                        }
                    }
                }
            });
        }
        isTarget = false;
    });

    // 代理处理keyup，处理回车事件，触发focusout
    editForm.delegate(".J_CanBlur", "keyup", function(e){
        if (e.keyCode === 13) {
            $(e.delegateTarget).find("input").first().trigger("focusout");
            $(".ui-horizontal").width($("#J_UICodePanel").width());
            return false;
        }
    });

    // 取消编辑界面a标签默认事件，编码编辑时误操作跳转
    canvas.delegate("a", "click", function(e){
        // return false;
        e.preventDefault();
    });

    // 特殊处理：背景组件
    $(".J_ToolBg").click(function(e){
        currentTarget.removeClass("active");
        relations[currentTarget.attr("id")] = editForm.html();
        if (formData[currentTarget.attr("id")]) {
            editForm.find("input[type='text']").each(function(i, o){
                formData[currentTarget.attr("id")][i] = $(o).val();
            });
        }
        currentTarget = $("#J_UICodePanel");
        editForm.html(relations[currentTarget.attr("id")]);
        // editForm.find("input[name='height']").val(currentTarget.height());
        // editForm.find("input[name='width']").val(currentTarget.width());
        editForm.find("input[type='text']").each(function(i, o){
            $(o).val(formData[currentTarget.attr("id")][i]);
        });
        deleteBar.css("display", "none");
    });

    // 批量绑定左侧工具点击操作
    $(".J_SimpleTool").click(function(e){
        currentTarget.removeClass("active");
        relations[currentTarget.attr("id")] = editForm.html();
        var nid = "J_UI_" + guid();
        var nNode = $($("#" + $(this).attr("data-ui-code")).html()).attr("id", nid).addClass("ui-can-edit").css({"left": "450px", "top": "0"});
        nNode.append(resizeMain);
        relations[nid] = $("#" + $(this).attr("data-ui-edit")).html();
        formData[nid] = [];
        editForm.html(relations[nid]);
        canvas.append(nNode);
        currentTarget = nNode;
        editForm.find("input[type='text']").each(function(i, o){
            formData[nid][i] = $(o).val();
        });
        currentTarget.addClass("active");
        deleteBar.css("display", "block");
    });

    // 拖拽功能
    var sorigin = {x: 0, y: 0}, // 记录鼠标按下时的坐标，初始化为(0, 0)
        morigin = {x: 0, y: 0}, // 记录鼠标按下后移动的坐标，初始化为(0, 0)
        drag_left = 0, // 拖动前的左边距
        drag_dtop = 0; // 拖动前的上边距
    canvas.delegate(".ui-drag-control", "mousedown", function(e){
        var canvas_width = canvas.width(),
            canvas_height = canvas.height(),
            cur_width, cur_height,
            left, top;

        isTarget = true;
        // 确保在focus执行完成之后再重置 isTarget
        isTargetTime = setTimeout(function(){
            isTarget = false;
        }, 100);
        currentTarget.removeClass("active");
        relations[currentTarget.attr("id")] = editForm.html();
        editForm.find("input[type='text']").each(function(i, o){
            formData[currentTarget.attr("id")][i] = $(o).val();
        });
        currentTarget = $(e.currentTarget).parents(".ui-can-edit");
        currentTarget.addClass("active");
        editForm.html(relations[currentTarget.attr("id")]);
        deleteBar.css("display", "block");

        cur_width = currentTarget.width(),
        cur_height = currentTarget.height(),
        sorigin.x = e.clientX;
        sorigin.y = e.clientY;
        drag_left = parseFloat(currentTarget.css("left"));
        drag_top = parseFloat(currentTarget.css("top"));
        // editForm.find("input[name='left']").val(drag_left);
        // editForm.find("input[name='top']").val(drag_top);
        // editForm.find("input[name='width']").val(currentTarget.width());
        // editForm.find("input[name='height']").val(currentTarget.height());
        editForm.find("input[type='text']").each(function(i, o){
            $(o).val(formData[currentTarget.attr("id")][i]);
        });
        canvas.bind("mousemove", function(ev){
            morigin.x = ev.clientX;
            morigin.y = ev.clientY;
            var dis_left = morigin.x - sorigin.x,
                dis_top = morigin.y - sorigin.y;
            left = drag_left + dis_left;
            if (left < 0) {
                left = 0;
            } else if (left > canvas_width - cur_width) {
                left = canvas_width - cur_width;
            }
            top = (drag_top + dis_top) < 0 ? 0 : (drag_top + dis_top);
            currentTarget.css({"left": left + "px", "top": top + "px"});
            editForm.find("input[name='left']").val(left);
            editForm.find("input[name='top']").val(top);
        }).bind("mouseup", function(ev){
            canvas.unbind("mousemove");
            canvas.unbind("mouseup");
            relations[currentTarget.attr("id")] = editForm.html();
        });
    });

    // 删除当前控件
    deleteBar.delegate(".J_DeleteCurrentTarget", "click", function(e){
        if (confirm("确定删除当前控件吗？")) {
            deleteBar.css("display", "none");
            relations[currentTarget.attr("id")] = undefined;
            formData[currentTarget.attr("id")] = undefined;
            currentTarget.remove();
            $(".J_ToolBg").trigger("click");
            e.preventDefault();
        }
    });

    // 清除画布
    $("#J_Clear").click(function(e){
        if (confirm("确定清除画布中的所有控件吗？")) {
            $("#J_UICodePanel").html("");
            relations = {
                "J_UICodePanel": $("#J_Edit_bg").html()
            };
        }
        $(".J_ToolBg").trigger("click");
    });

    // 复制代码
    var client = new ZeroClipboard(document.getElementById("J_Copy"), {
        moviePath: "./images/ZeroClipboard.swf"
    });

    client.on("mouseDown", function(){
        var html = $("#J_UICodePanel").html();
        html = html.replace(/<div class="ui-drag-main">(?:\r|\n|.)*?ui-resize-rightbottom"><\/div>[\r\n]\s*<\/div>/g, "");
        html = html.replace(/\sid="[\w_]+"/g, "");
        client.setText(html);
        alert("复制代码成功，Ctrl+V粘帖");
    });

    // 代理画布上的所有控件，点击时设置currentTarget
    // canvas.delegate(".ui-drag-control", "mousedown", function(e){
    //     if (!moved) {
    //         currentTarget.removeClass("active");
    //         relations[currentTarget.attr("id")] = editForm.html();
    //         currentTarget = $(e.currentTarget).parents(".ui-can-edit");
    //         currentTarget.addClass("active");
    //         editForm.html(relations[currentTarget.attr("id")]);
    //     }
    // });

    // resize功能分8段，应该还可以优化
    var osize = {width: 0, height: 0};
    canvas.delegate(".ui-resize-lefttop", "mousedown", function(e){
        sorigin.x = e.clientX;
        sorigin.y = e.clientY;
        drag_left = parseFloat(currentTarget.css("left"));
        drag_top = parseFloat(currentTarget.css("top"));
        osize.width = currentTarget.width();
        osize.height = currentTarget.height();
        canvas.bind("mousemove", function(ev){
            morigin.x = ev.clientX;
            morigin.y = ev.clientY;
            var dis_left = morigin.x - sorigin.x,
                dis_top = morigin.y - sorigin.y;
            var left = drag_left + dis_left,
                width = osize.width - dis_left,
                top = (drag_top + dis_top) < 0 ? 0 : (drag_top + dis_top),
                height = osize.height - dis_top;
            if (left < 0) {
                left = 0;
            }
            currentTarget.css({"left": left + "px", "top": top + "px", "width": width + "px", "height": height + "px"});
            editForm.find("input[name='left']").val(left);
            editForm.find("input[name='top']").val(top);
            editForm.find("input[name='width']").val(width);
            editForm.find("input[name='height']").val(height);
        }).bind("mouseup", function(ev){
            canvas.unbind("mousemove");
            canvas.unbind("mouseup");
        });
    });
    canvas.delegate(".ui-resize-centertop", "mousedown", function(e){
        sorigin.y = e.clientY;
        drag_top = parseFloat(currentTarget.css("top"));
        osize.height = currentTarget.height();
        canvas.bind("mousemove", function(ev){
            morigin.y = ev.clientY;
            var dis_top = morigin.y - sorigin.y;
            var top = (drag_top + dis_top) < 0 ? 0 : (drag_top + dis_top),
                height = osize.height - dis_top;
            currentTarget.css({"top": top + "px", "height": height + "px"});
            editForm.find("input[name='top']").val(top);
            editForm.find("input[name='height']").val(height);
        }).bind("mouseup", function(ev){
            canvas.unbind("mousemove");
            canvas.unbind("mouseup");
        });
    });
    canvas.delegate(".ui-resize-righttop", "mousedown", function(e){
        sorigin.x = e.clientX;
        sorigin.y = e.clientY;
        drag_top = parseFloat(currentTarget.css("top"));
        osize.width = currentTarget.width();
        osize.height = currentTarget.height();
        canvas.bind("mousemove", function(ev){
            morigin.x = ev.clientX;
            morigin.y = ev.clientY;
            var dis_left = morigin.x - sorigin.x,
                dis_top = morigin.y - sorigin.y;
            var width = osize.width + dis_left,
                top = (drag_top + dis_top) < 0 ? 0 : (drag_top + dis_top),
                height = osize.height - dis_top;
            currentTarget.css({"top": top + "px", "width": width + "px", "height": height + "px"});
            editForm.find("input[name='top']").val(top);
            editForm.find("input[name='width']").val(width);
            editForm.find("input[name='height']").val(height);
        }).bind("mouseup", function(ev){
            canvas.unbind("mousemove");
            canvas.unbind("mouseup");
        });
    });
    canvas.delegate(".ui-resize-rightbottom", "mousedown", function(e){
        sorigin.x = e.clientX;
        sorigin.y = e.clientY;
        osize.width = currentTarget.width();
        osize.height = currentTarget.height();
        canvas.bind("mousemove", function(ev){
            morigin.x = ev.clientX;
            morigin.y = ev.clientY;
            var dis_left = morigin.x - sorigin.x,
                dis_top = morigin.y - sorigin.y;
            var width = osize.width + dis_left,
                height = osize.height + dis_top;
            currentTarget.css({"width": width + "px", "height": height + "px"});
            editForm.find("input[name='width']").val(width);
            editForm.find("input[name='height']").val(height);
        }).bind("mouseup", function(ev){
            canvas.unbind("mousemove");
            canvas.unbind("mouseup");
        });
    });
    canvas.delegate(".ui-resize-centerbottom", "mousedown", function(e){
        sorigin.y = e.clientY;
        osize.height = currentTarget.height();
        canvas.bind("mousemove", function(ev){
            morigin.y = ev.clientY;
            var dis_top = morigin.y - sorigin.y;
            var height = osize.height + dis_top;
            currentTarget.css({"height": height + "px"});
            editForm.find("input[name='height']").val(height);
        }).bind("mouseup", function(ev){
            canvas.unbind("mousemove");
            canvas.unbind("mouseup");
        });
    });
    canvas.delegate(".ui-resize-leftbottom", "mousedown", function(e){
        sorigin.x = e.clientX;
        sorigin.y = e.clientY;
        drag_left = parseFloat(currentTarget.css("left"));
        osize.width = currentTarget.width();
        osize.height = currentTarget.height();
        canvas.bind("mousemove", function(ev){
            morigin.x = ev.clientX;
            morigin.y = ev.clientY;
            var dis_left = morigin.x - sorigin.x,
                dis_top = morigin.y - sorigin.y;
            var left = drag_left + dis_left,
                width = osize.width - dis_left,
                height = osize.height + dis_top;
            if (left < 0) {
                left = 0;
            }
            currentTarget.css({"left": left + "px", "width": width + "px", "height": height + "px"});
            editForm.find("input[name='left']").val(left);
            editForm.find("input[name='width']").val(width);
            editForm.find("input[name='height']").val(height);
        }).bind("mouseup", function(ev){
            canvas.unbind("mousemove");
            canvas.unbind("mouseup");
        });
    });
    canvas.delegate(".ui-resize-leftcenter", "mousedown", function(e){
        sorigin.x = e.clientX;
        drag_left = parseFloat(currentTarget.css("left"))
        osize.width = currentTarget.width();
        canvas.bind("mousemove", function(ev){
            morigin.x = ev.clientX;
            var dis_left = morigin.x - sorigin.x;
            var left = drag_left + dis_left,
                width = osize.width - dis_left;
            if (left < 0) {
                left = 0;
            }
            currentTarget.css({"left": left + "px", "width": width + "px"});
            editForm.find("input[name='left']").val(left);
            editForm.find("input[name='width']").val(width);
        }).bind("mouseup", function(ev){
            canvas.unbind("mousemove");
            canvas.unbind("mouseup");
        });
    });
    canvas.delegate(".ui-resize-rightcenter", "mousedown", function(e){
        sorigin.x = e.clientX;
        osize.width = currentTarget.width();
        canvas.bind("mousemove", function(ev){
            morigin.x = ev.clientX;
            var dis_left = morigin.x - sorigin.x;
            var width = osize.width + dis_left;
            currentTarget.css({"width": width + "px"});
            editForm.find("input[name='width']").val(width);
        }).bind("mouseup", function(ev){
            canvas.unbind("mousemove");
            canvas.unbind("mouseup");
        });
    });
});